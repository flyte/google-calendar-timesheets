import httplib2
import os
import re
import socks
from datetime import datetime
from dateutil import parser, tz

from apiclient.discovery import build
from oauth2client.file import Storage
from oauth2client.client import AccessTokenRefreshError
from oauth2client.client import flow_from_clientsecrets
from oauth2client.tools import run

class GoogleCalendar:
	"""
	Leverages Google's client API to perform various functions using Google Calendar
	"""

	SCOPE = "https://www.googleapis.com/auth/calendar.readonly"
	STORAGE_NAME = "credentials-calendar.dat"
	MISSING_CLIENT_SECRETS_MESSAGE = """
WARNING: Please configure OAuth 2.0

To make this sample run you will need to populate the client_secrets.json file
found at:

%s

with information from the APIs Console <https://code.google.com/apis/console>.

""" % os.path.join(os.path.dirname(__file__), "client_secrets.json")

	client_secrets_file = None
	http = None
	service = None
	time_format = "%Y-%m-%dT%H:%M:%SZ"
	time_format_nonutc = "%Y-%m-%dT%H:%M:%S%z"
	ext_time_format = "%Y-%m-%dT%H:%M:%S.%fZ"
	ext_time_format_nonutc = "%Y-%m-%dT%H:%M:%S.%f%z"

	def __init__(self, client_secrets_file = "client_secrets.json"):
		self.client_secrets_file = client_secrets_file

	def format_datetime(self, datetime):
		return datetime.isoformat("T") + "Z"
		
	def parse_datetime(self, datetimestring, extended = False):
		return parser.parse(datetimestring)
		# If it's UTC
		if datetimestring[len(datetimestring) - 1] == "Z":
			if extended:
				return datetime.strptime(datetimestring, self.ext_time_format)
			else:
				return datetime.strptime(datetimestring, self.time_format)
		# If it's got an offset
		else:
			match = re.match(r".+?(\d{2}):(\d{2})$", datetimestring)
			hour = minute = None
			try:
				hour = match.group(1)
				minute = match.group(2)
			except (IndexError, AttributeError):
				raise ValueError("Datetime string was not in UTC (with a Z at the end) and we're unable to parse the offset value (eg. +01:00) at the end of the string: %s" % datetimestring)
			newdatetimestring = "%s%s%s" % (datetimestring[len(datetimestring) - 5], hour, minute)
			if extended:
				return datetime.strptime(newdatetimestring, self.ext_time_format_nonutc).astimezone(tz.tzutc())
			else:
				return datetime.strptime(newdatetimestring, self.time_format_nonutc).astimezone(tz.tzutc())

	def authenticate(self):
		"""Attempt to use stored credentials else authorize and store them."""

		# Set up a Flow object to be used if we need to authenticate.
		flow = flow_from_clientsecrets(self.client_secrets_file,
			scope=self.SCOPE, message=self.MISSING_CLIENT_SECRETS_MESSAGE)

		# If the Credentials don't exist or are invalid run through the native client
		# flow. The Storage object will ensure that if successful the good
		# Credentials will get written back to a file.
		storage = Storage(self.STORAGE_NAME)
		credentials = storage.get()
		if credentials is None or credentials.invalid:
			credentials = run(flow, storage)

		# Create an httplib2.Http object to handle our HTTP requests and authorize it
		# with our good Credentials.
		h = httplib2.Http(
			proxy_info = httplib2.ProxyInfo(socks.PROXY_TYPE_HTTP, 'proxy.imagination.com', 80))
		h = httplib2.Http()
		self.http = credentials.authorize(h)

		self.service = build("calendar", "v3", http=self.http)

	def get_calendar_list(self):
		"""Returns a list of calendars."""
		self.authenticate()
		return self.service.calendarList().list().execute(http=self.http)

	def search_calendar_id(self, calendar_name):
		"""Returns a calendar ID based on the calendar name (or "summary")."""
		self.authenticate()
		calendars = self.get_calendar_list()
		for calendar in calendars["items"]:
			if calendar["summary"].lower() == calendar_name.lower():
				return calendar["id"]

	def get_calendar(self, calendar_id):
		"""Returns a single calendar."""
		self.authenticate()
		return self.service.calendarList().get(calendarId=calendar_id).execute(http=self.http)

	def get_events(self, calendar_id, since=None):
		"""Returns all events on the specified calendar."""
		self.authenticate()

		if since:
			events = self.service.events().list(
				calendarId=calendar_id,
				timeMin=since.isoformat("T")+"Z"
				).execute(http=self.http)
		else:
			events = self.service.events().list(calendarId=calendar_id).execute(http=self.http)

		while "nextPageToken" in events:
			more_events = self.service.events().list(
				calendarId=calendar_id, pageToken=events["nextPageToken"]).execute(http=self.http)
			events["items"] += more_events["items"]
			if "nextPageToken" in more_events:
				events["nextPageToken"] = more_events["nextPageToken"]
			else:
				del events["nextPageToken"]
		
		return events

	def get_events_since_last_updated(self, calendar_id, last_updated):
		"""
		Returns all events on the specified calendar which have updated since the supplied datetime.
		
		calendar_id -- The calendar ID
		last_updated -- datetime: The oldest update time we're interested in seeing
		"""
		self.authenticate()
		last_updated_string = self.format_datetime(last_updated)
		return self.service.events().list(
			calendarId=calendar_id,
			updatedMin=last_updated.isoformat("T")
		).execute(http=self.http)

