import fisix
from datetime import datetime, timedelta
from argparse import ArgumentParser

p = ArgumentParser()
p.add_argument("days", type=int)

if __name__ == "__main__":
	args = p.parse_args()

	api = fisix.FisixApi()
	new_datetime = datetime.now() - timedelta(days=args.days)
	api.save_last_updated(new_datetime)