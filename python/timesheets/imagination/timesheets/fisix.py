from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import time, re, sys
from collections import defaultdict
from datetime import datetime, timedelta
import cPickle as pickle
from dateutil import tz
from time import sleep

class StaticWorkError(Exception):
	def __init__(self, day, month, year, work, statics,
			message = "There is work already submitted to Fisix which has become static, but doesn't match any of the jobs we're trying to input. You will have to check Fisix manually and possibly contact finance to resolve."):
		self.day = day
		self.month = month
		self.year = year
		self.work = work
		self.statics = statics
		self.message = message

class FisixApi:
	"""Stores time data and enters it into the Fisix timesheet system using Selenium"""

	username = None
	password = None
	driver = None
	submit_updates_from_datetime = None

	logged_in = False
	data = defaultdict(list)
	last_updated_file = "lastupdated.dat"
	base_url = "http://fisix.imagination.com/"

	def initialise_selenium(self):
		"""Set up selenium"""
		self.driver = webdriver.Firefox()
		self.driver.implicitly_wait(10)
		
	def log_in(self):
		"""Open login page, enter the username and password."""
		if not self.username or not self.password:
			print "Cannot continue without username and password."
			return
		
		print "Getting login page."
		self.driver.get(self.base_url + "/login.php?page=%2Findex.php")

		print "Entering username."
		self.driver.find_element_by_name("username").clear()
		self.driver.find_element_by_name("username").send_keys(self.username)

		print "Entering password."
		self.driver.find_element_by_name("password").clear()
		self.driver.find_element_by_name("password").send_keys(self.password)

		print "Logging in."
		self.driver.find_element_by_name("Login").click()

		self.logged_in = True

	def submit_day(self, day, month, year, work):
		"""
		Submits a days worth of time data to Fisix.

		day -- The day
		month -- The month
		year -- The year
		work -- A list of hashes which represent the work done. See get_data() for format.
		"""

		# Load the browser and log in to Fisix if haven't done already
		if not self.logged_in:
			self.initialise_selenium()
			self.log_in()

		# Load the day on Fisix
		print "Getting page for %s.%s.%s." % (day, month, year)
		self.driver.get(self.base_url + "/time/index.php?day=%s&month=%s&year=%s" % (day, month, year))

		# Pull all entries for this day that have become static
		table = self.driver.find_element_by_xpath("//td[starts-with(., 'Total')]/../..")
		
		other_field_count = 3
		entry_field_count = 8
		static_field_count = len(table.find_elements_by_tag_name("tr")) - (entry_field_count + other_field_count)
		
		statics = []
		for i in range(3, (static_field_count + 3)):
			tr = self.driver.find_element_by_xpath("//td[starts-with(., 'Total')]/../../tr[%d]" % i)
			tds = tr.find_elements_by_tag_name("td")
			statics.append({ "job": tds[0].text.split(" ")[0], "activity": tds[1].text, "duration": tds[2].text })
		
		# Take any entries out of 'work' if they match the static entries (j/n, time and activity)
		# and add them to the static_work list.
		static_work = []
		work_to_add = []
		for w in work:
			in_statics = False
			for s in statics:
				if w["job"] == s["job"] and w["duration"] == s["duration"]:
					static_work.append(w)
					in_statics = True
			if not in_statics:
				work_to_add.append(w)
		
		# If we have any static entries left which don't match what we have in 'work' then error	
		if len(statics) > len(static_work):
			raise StaticWorkError(day, month, year, work, statics)
		
		# Fill in all work for this day
		i = 1
		for w in work_to_add:
			print "Filling in data for job %s." % i
			name = "job%s" % i
			self.driver.execute_script(
				"document.getElementsByName('%s')[0].setAttribute('maxlength', '8')" % name)
			self.driver.find_element_by_name(name).clear()
			self.driver.find_element_by_name(name).send_keys(w["job"])
			Select(self.driver.find_element_by_name("act%s" % i)).select_by_visible_text(w["activity"])
			Select(self.driver.find_element_by_name("dur%s" % i)).select_by_visible_text(w["duration"])
			self.driver.find_element_by_name("narr%s" % i).clear()
			self.driver.find_element_by_name("narr%s" % i).send_keys(w["description"])
			i += 1

		print "Clearing all other fields."
		while i <= 8:
			self.driver.find_element_by_name("job%s" % i).clear()
			Select(self.driver.find_element_by_name("dur%s" % i)).select_by_visible_text("")
			i += 1

		# Click save
		print "Saving time.."
		self.driver.find_element_by_xpath("//button[@onclick='return doSave()']").click()

		# Make sure the changes were saved
		try:
			print self.driver.find_element_by_css_selector("div.good").text
			return True
		except NoSuchElementException, e:
			print e
			print "Please see browser window for more information."
			return False

	def set_username(self, username, password = None):
		"""
		Set the username to be used when logging in to fisix.

		username -- The username
		password -- The password (default None)
		"""
		self.username = username
		if password:
			self.password = password

	def read_pw_from_file(self, file):
		"""
		Read the Fisix login password from the first line of a plaintext file.

		file -- The file to read from
		"""
		try:
		    self.password = open(file, "r").readline().strip()
		except Exception, e:
		    print >> sys.stderr, "FATAL: Unable to open password file at %s. Please create this file with your Fisix password in plaintext." % file
		    print >> sys.stderr, "Inner exception: %s" % e
		    sys.exit(1)
		    

	def get_key(self, day, month, year):
		"""
		Return the day, month and year in "day,month,year" format to use as a
		key for storing the timesheet data.
		"""
		return "%s,%s,%s" % (day, month, year)

	def add_time_to_day(self, day, month, year, job, duration,
						activity = "Media Group",
						updated = datetime.utcnow().replace(tzinfo=tz.tzutc()),
						description = ""):
		"""
		Add the given amount of time to the specified day. If the day has no time
		yet then the key will be created automatically (the 'data' variable is a
		'defaultdict' set up to create a 'list' object.

		day -- The day
		month -- The month
		year -- The year
		job -- The job number
		duration -- The amount of time spent
		activity -- Which department worked for (default "Media Group")
		updated -- datetime: When the job was last updated in RFC 3339 format (default datetime.utcnow())
		description -- What to enter into the narrative field
		"""
		key = self.get_key(day, month, year)
		self.data[key].append({
			"job": job,
			"activity": activity,
			"duration": duration,
			"updated": updated,
			"description": description
		})

	def get_data(self):
		"""
		Returns the hash of all time stored so far in the following format:
		{
		  "<day>,<month>,<year>": [
		    { "job": "<job1>", "activity": "<activity1>", "duration": "<duration1>", "updated": "<updated1>", "description": "<description1>" },
		    { "job": "<job2>", "activity": "<activity2>", "duration": "<duration2>", "updated": "<updated2>", "description": "<description2>" }
		  ]
		}
		"""
		return self.data

	def save_last_updated(self, thedatetime = None):
		"""
		Saves a file with the current datetime which we can check later in
		order to ascertain what data we need to send to Fisix
		"""
		with open(self.last_updated_file, 'w') as f:
			if not thedatetime:
				pickle.dump(datetime.utcnow().replace(tzinfo=tz.tzutc()), f)
			else:
				pickle.dump(thedatetime.replace(tzinfo=tz.tzutc()), f)
		
	def get_last_updated(self):
		"""
		Reads the last updated datetime from a file and returns it as a datetime object.
		If it's unable to read the file, it'll return the minimum representable date.
		"""
		try:
			with open(self.last_updated_file, 'r') as f:
				return pickle.load(f)
		except IOError:
			return datetime.min.replace(tzinfo=tz.tzutc())

	def set_last_updated(self):
		"""
		Get the last updated time and set that as the one we'll use to establish
		which jobs need updating. Immediately save now() as the last updated time
		so that the next time we check, we won't miss any changes that happened
		on Google Calendar between pulling these events and saving a new last_updated
		time.
		"""
		self.submit_updates_from_datetime = self.get_last_updated()
		self.datetime_last_updated = datetime.now()
		#self.save_last_updated()

	def make_it_so(self):
		"""Pushes all stored time to Fisix."""
		
		if len(self.data) < 1:
			print "Cowardly refusing to enter timesheets without any data."
			return
	
		# Find out when we last updated Fisix. This should be called from outside of this
		# class just before the timesheet data is pulled from its source (Google Calendar)
		if self.submit_updates_from_datetime is None:
			self.submit_updates_from_datetime = self.get_last_updated()
			self.datetime_last_updated = datetime.now()
			#self.set_last_updated()

		errors = []

		try:
			# For each day's worth of data
			for day, work in self.data.iteritems():
				d, m, y = day.split(",")
				
				# Figure out if this day needs updating
				to_update = []
				i = 1
				for w in work:
					if w["updated"] >= self.submit_updates_from_datetime:
						#print "Adding %s to the work queue because it's greater than or equal to %s." % (w["updated"], self.submit_updates_from_datetime)
						to_update.append(i)
					i += 1
				
				if len(to_update) == 0:
					print "Nothing to update for %s.%s.%s. Moving on.." % (d, m, y)
					continue

				# Load the browser and log in to Fisix if haven't done already
				if not self.logged_in:
					self.initialise_selenium()
					self.log_in()
				
				try:
					if not self.submit_day(d, m, y, work):
						cont = raw_input("Error submitting timesheets. Do you wish to continue? (y/n)")
						if cont.lower() != "y":
							return
				except StaticWorkError, e:
					errors.append(e)

			print "All timesheets filled in!"
			if len(errors) > 0:
				print "However, there were some errors:"
				print errors[0].message
				for e in errors:
					print "Day:Month:Year: %s:%s:%s" % (e.day, e.month, e.year)
					print "\tWork on Google Calendar:\n\t\t%s" % e.work
					print "\tWork which has become static (unchangeable):\n\t\t%s" % e.statics
			
			if self.driver is not None:
				self.driver.quit()

			self.save_last_updated(self.datetime_last_updated)

		except Exception, e:
			print "Overall failure: %s", e