from googlecalendar import GoogleCalendar
from fisix import FisixApi
from datetime import datetime
import sys

gcal = GoogleCalendar()
gcal.authenticate()

fisix = FisixApi()
fisix.set_username("eper")
fisix.read_pw_from_file("pw.txt")

cid = gcal.search_calendar_id("Time Sheets")
#events = gcal.get_events(cid)
events = gcal.get_events_since_last_updated(cid, fisix.get_last_updated())

for event in events["items"]:
	if "updated" not in event:
		continue
	updated = gcal.parse_datetime(event["updated"], extended = True)
	start = gcal.parse_datetime(event["start"]["dateTime"])
	end = gcal.parse_datetime(event["end"]["dateTime"])
	diff = end - start
	hours, remainder = divmod(diff.seconds, 3600)
	minutes, seconds = divmod(remainder, 60)

	jn = event["summary"]
	description = event.get("description", "")
	day = "%02d" % start.day
	month = "%02d" % start.month
	year = "%04d" % start.year
	date = "%s,%s,%s" % (day, month, year)
	time = "%d:%02d" % (hours, minutes)

	print "J/N: %s" % jn
	print "Date: %s" % date
	print "Time: %s" % time
	print "----------------"

	fisix.add_time_to_day(day, month, year, jn, time, updated=updated, description=description)

#print fisix.get_data()
#print ""

#cont = raw_input("Would you like to enter this into Fisix? (y/n)..")
#if cont == "y":
raw_input("Events have been downloaded from Google Calendar. Press Enter to input them to Fisix...")
fisix.make_it_so()