from googlecalendar import GoogleCalendar
from datetime import datetime, timedelta
from prettytable import PrettyTable
from workalendar.europe import UnitedKingdom
import sys
import argparse

p = argparse.ArgumentParser()
p.add_argument("--from_days_ago", default=0, type=int)
p.add_argument("--plain", default=False, action="store_true")

if __name__ == "__main__":
	args = p.parse_args()
	since = None
	if args.from_days_ago > 0:
		since = datetime.now() - timedelta(days=args.from_days_ago)

	x = PrettyTable(("Date", "Job", "Hours", "Hours Over", "Weekday"))
	x.align["Weekday"] = "l"
	x.align["Job"] = "l"

	gcal = GoogleCalendar()
	gcal.authenticate()

	cid = gcal.search_calendar_id("Time Sheets")
	events = gcal.get_events(cid, since)

	uk = UnitedKingdom()
	
	overtime = 0
	dates = {}
	for event in events["items"]:
		updated = gcal.parse_datetime(event["updated"], extended = True)
		start = gcal.parse_datetime(event["start"]["dateTime"])
		end = gcal.parse_datetime(event["end"]["dateTime"])
		diff = end - start
		hours, remainder = divmod(diff.seconds, 3600)
		minutes, seconds = divmod(remainder, 60)
		
		jn = event["summary"]
		day = "%02d" % start.day
		month = "%02d" % start.month
		year = "%04d" % start.year
		date = "%s,%s,%s" % (day, month, year)
		time = "%d:%02d" % (hours, minutes)
		
		if dates.has_key(date):
			dates[date]["hours"] += hours
			dates[date]["minutes"] += minutes
		else:
			dates[date] = { "hours": hours, "minutes": minutes, "start_datetime": start, "job_number": jn }

	for d in sorted(dates, key=lambda x: datetime.strptime(x, "%d,%m,%Y")):
		hours = dates[d]["hours"]
		minutes = dates[d]["minutes"]
		start = dates[d]["start_datetime"]
		jn = dates[d]["job_number"]

		hours_today = float(hours + (minutes / 60.0))
		
		# @TODO: This doesn't account for half-days of TOIL
		if jn[:4] == "9085":
			sys.stdout.write("%s:\t" % "/".join(d.split(",")))
			sys.stdout.write("Removing %d hours of taken TOIL.\n" % hours_today)
			overtime -= hours_today
			continue

		workday = uk.is_working_day(start.date())
		if workday and hours_today >= 13:
			hours_over = 4
		elif not workday and hours_today >= 4:
			hours_over = 8
		elif not workday and hours_today > 1 and hours_today < 4:
			hours_over = 4
		else:
			hours_over = 0

		weekdays = [ "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" ]

		if hours_over > 0:
			x.add_row((
				"/".join(d.split(",")),
				jn,
				"%02d:%02d" % (hours, minutes),
				hours_over,
				weekdays[start.isoweekday() - 1]
			))
			if args.plain:
				sys.stdout.write("%s\t" % "/".join(d.split(",")))
				sys.stdout.write("%s\t" % jn)
				sys.stdout.write("%02d:%02d\t" % (hours, minutes))
				sys.stdout.write("%s\t" % hours_over)
				sys.stdout.write("%s\n" % weekdays[start.isoweekday() - 1])
			
		overtime += hours_over

	if not args.plain:
		print x
	print "Hours over: %s" % overtime
	print "Days: %s" % (overtime / 8)
